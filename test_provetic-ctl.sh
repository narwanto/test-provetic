#!/bin/bash

WORKING_DIR="/home/oracle"
APP="test-provetic"
VERSION="1.0.0"
APP_NAME="$WORKING_DIR/$APP-$VERSION.jar"
ROOT_APP="$WORKING_DIR/"

CONF="-server -XX:MaxNewSize=64m -XX:NewSize=64m -Xms256m -Xmx256m"

JVM_ARGSD="$CONF -Dspring.profiles.active=development"
JVM_ARGSP="$CONF -Dspring.profiles.active=production"
JVM_ARGST="$CONF -Dspring.profiles.active=testing"

if [ "$1" ]; then

        if [ "$1" = "start" ];then
                nohup java -DROOT_APP=$ROOT_APP -jar $APP_NAME </dev/null >/dev/null 2>&1 &
                echo "starting $APP_NAME..."
        elif [ "$1" = "stop" ]; then
                echo "stopping $APP_NAME..."
                pkill -f $APP
                echo "$APP-$VERSION successfully stopped."
        fi

else
    echo "Please give first argument 'start' or 'stop'"
fi
