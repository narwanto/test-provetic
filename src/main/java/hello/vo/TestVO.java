package hello.vo;

import lombok.Data;

@Data
public class TestVO {
    private String url;
    private String format;
    private String token;

}
