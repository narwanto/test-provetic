package hello.service;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

@Service
public class JsonFromUrlService {
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException {
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(jsonText);
            return (JSONObject) obj;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    public static JSONObject getJsonFromUrl(String url, String token) throws IOException {
        URL myURL = new URL(url);
        HttpURLConnection connection = (HttpURLConnection)myURL.openConnection();
        connection.setRequestMethod("GET");
        if(token!=null)connection.setRequestProperty("token",token);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        System.out.println("respone message: "+connection.getResponseMessage());
        if(connection.getResponseMessage().equals("OK")) {
            InputStream inStrm = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inStrm, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONParser parser = new JSONParser();
            Object obj = null;
            try {
                obj = parser.parse(jsonText);
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                inStrm.close();
            }
            return (JSONObject) obj;
        }
        else return new JSONObject();
    }


}
