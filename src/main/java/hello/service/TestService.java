package hello.service;

import hello.vo.ResultVO;
import hello.vo.TestVO;
import hello.vo.XdibiResponseVO;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TestService {

    private Logger logger = LoggerFactory.getLogger(TestService.class);


    private XdibiResponseVO hitBnpb(String url, String generateToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("token", generateToken);

        HttpEntity<String> payloadHttpEntity = new HttpEntity<>(headers);
        XdibiResponseVO resultVO = new XdibiResponseVO();
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
            HttpMessageConverter stringHttpMessageConverternew = new StringHttpMessageConverter();
            restTemplate.setMessageConverters(Arrays.asList(new HttpMessageConverter[]{formHttpMessageConverter, stringHttpMessageConverternew}));

            resultVO = restTemplate.postForObject(url, payloadHttpEntity, XdibiResponseVO.class);
        } catch (Exception e) {
            return resultVO;
        }
        return resultVO;
    }

    public ResultVO hitLinkUrlParam(String url,String format){
        TestVO testVO = new TestVO();
        testVO.setFormat(format);
        testVO.setUrl(url);
        return hitLinkUrl(testVO);
    }
    public ResultVO hitHardcode(String fromDate,String toDate){
        TestVO testVO = new TestVO();
        String url = "http://trustpositif.kominfo.go.id/api/data.php?r=100&dend="+toDate+"&dstart="+fromDate;
        testVO.setUrl(url);
        testVO.setFormat("json");
        return hitLinkUrl(testVO);
    }
    public ResultVO hitLinkUrl(TestVO vo){
        String format = vo.getFormat();
        String link = vo.getUrl();
        logger.error("ini linknya"+link);

        ResultVO resultVO = new ResultVO();
        if(format==null || format.equals("")) resultVO.setMessage("format harus diisi");
        else {
            if(format.equals("json")){
                JSONObject result = null;
                try {
                    result = JsonFromUrlService.readJsonFromUrl(link);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(null == result) resultVO.setMessage("url bermasalah");
                else resultVO.setMessage("OK");
                resultVO.setResult(result);
            }
            else resultVO.setMessage("unknown format");
        }
        System.out.println("hasil hit url :"+resultVO);
        return resultVO;
    }

    public ResultVO getDataKemnaker(){
        String urlKemnaker = "https://geospasial.kemnaker.go.id/server/rest/services/INFO_KERJAService/MapServer/0/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
        ResultVO resultVO = new ResultVO();
        JSONObject jsonResult;
        try {
            jsonResult = JsonFromUrlService.readJsonFromUrl(urlKemnaker);
            resultVO.setMessage("OK");
            resultVO.setResult(jsonResult);
        } catch (IOException e) {
            resultVO.setMessage("500");
            resultVO.setResult("ERROR");
        }
        return resultVO;
    }

    public JSONObject getDataKemnakerJSON(){
        String urlKemnaker = "https://geospasial.kemnaker.go.id/server/rest/services/INFO_KERJAService/MapServer/0/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
        try {
            return JsonFromUrlService.readJsonFromUrl(urlKemnaker);
        } catch (IOException e) {
            return new JSONObject();
        }
    }
    public JSONObject getDataFull(){
        String urlKemnaker = "http://geospasial.kemnaker.go.id/server/rest/services/INFO_KERJAService/MapServer/0/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson";
        JSONObject hasil = new JSONObject();
        try {
            JSONObject h = JsonFromUrlService.readJsonFromUrl(urlKemnaker);
            JSONArray features = (JSONArray) h.get("features");
            List<JSONObject> listAtt = new ArrayList<>();
            for (Object feature : features) {
                JSONObject feature1 = (JSONObject) feature;
                JSONObject att = (JSONObject) feature1.get("attributes");
                listAtt.add(att);
            }
            hasil.put("data",listAtt);
        } catch (IOException e) {
            hasil.put("data","ERROR");
        }
        return hasil;
    }

}
