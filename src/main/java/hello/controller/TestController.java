package hello.controller;

import hello.service.TestService;
import hello.vo.ResultVO;
import hello.vo.TestVO;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/v1/")
public class TestController {


    @Autowired
    TestService testService;

    @RequestMapping(value = "/get-from-link",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultVO hitLinkUrl(  @RequestParam(value = "url", required = true)String url,
                                    @RequestParam(value = "format", required = true)String format) {

        return testService.hitLinkUrlParam(url,format);
    }

    @RequestMapping(value = "/get-from-link-body",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultVO hitLinkUrlUrlBody(@RequestBody TestVO vo) {

        return testService.hitLinkUrl(vo);
    }

    @RequestMapping(value = "/get-kemkominfo",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultVO hitHardcode(@RequestParam(value = "fromDate", required = true)String fromDate,
                                @RequestParam(value = "toDate", required = true) String toDate
                                ) {
        return testService.hitHardcode(fromDate,toDate);
    }

    @RequestMapping(value = "/get-pencaker",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultVO hitKemnaker() {
        return testService.getDataKemnaker();
    }

    @RequestMapping(value = "/get-pencaker-json",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public JSONObject hitKemnakerJson() {
        return testService.getDataKemnakerJSON();
    }
    @RequestMapping(value = "/get-pencaker-full",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public JSONObject hitKemnakerFull() {
        return testService.getDataFull();
    }


}
